## Техническое задание
### Мониторинг контейнера с nginx на раборту

Ранее собранный контейнер с nginx запущен "где то".  
По пути 4toto.tam/monitoring доступен мониторинг соединений самого nginx.  
Пример предоставляемых метрик:  
```
Active connections: 1 
server accepts handled requests
 1 1 1 
Reading: 0 Writing: 1 Waiting: 0 
```

Как мы видим, формат вывода метрик не подходит для сбора при помощи prometheus.  
Для мониторинга рекомендуется использовать nginx exporter.  
Репозиторий экспортера доступен [тут](https://github.com/nginxinc/nginx-prometheus-exporter) 
(Обращаем внимание на параметр запуска. А именно -nginx.scrape-uri)   

После настройки экспортера, необходимо добавить job в конфигурационный файл prometheus - prometheus.yml  
Пример job:  
```
- job_name: 'nginx'
    static_configs:
      - targets: 
          - 127.0.0.1:9113

```

Пример расчитан на то, что nginx exporter работает на том же инстансе, что и prometheus. 

### Удачи, пилот!